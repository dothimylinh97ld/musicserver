const musicTempController = require("./../Controllers/musicTemp.controller");
const router = require("express").Router();

module.exports = function() {
    router.post("/musicTemp/create", musicTempController.createMusicTemp);
    router.get("/musicTemp", musicTempController.getAllmusic);
    return router;
}