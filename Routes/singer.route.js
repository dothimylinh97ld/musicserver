var singerController = require("./../Controllers/singer.controller");
var singer = require('../Models/singer.model');
var router = require("express").Router();

module.exports = function() {
    router.post("/singer/create", singerController.createSinger);
    router.get("/singer", singerController.getAllSinger);
    router.put("/singer/update/:id", singerController.updateSinger);
    router.get("/singer/:id", singerController.getOneSinger);
    router.delete("/singer/delete/:id", singerController.deleteSinger);
    router.get("/singer/music/:id", singerController.getSingerByMusic);
    return router;
}