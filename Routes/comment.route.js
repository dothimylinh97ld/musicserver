const router = require("express").Router();
const commentController = require("./../Controllers/comment.controller");
module.exports = function() {
    router.post("/comment/create", commentController.createComment);
    router.get("/comment/music/:id", commentController.getCommentbyMusic);
    router.get("/comment/user/:id", commentController.getCommentByUser);
    router.put("/comment/update/:id", commentController.updateComment);
    return router;
}