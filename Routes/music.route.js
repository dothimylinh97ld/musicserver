const musicController = require("./../Controllers/music.controller");
const commentController = require("./../Controllers/comment.controller");
// const multer = require('multer');
const music = require('../Models/music.model');
const router = require("express").Router();
const musicService = require('./../Services/music.service');
const typeService = require('./../Services/music.service');
// const storage = multer.diskStorage({
//     destination: function(req, file, cb) {
//         //biến cb kiểm tra xem  file có được chấp nhận để lưu trữ hay k?    
//         cb(null, './uploads/images'); //Định nghĩa nơi update file được lưu lại
//     },
//     filename: function(req, file, cb) { //đặt tên cho file
//         cb(null, Date.now() + '-' + file.originalname);
//     }
// });
// const fileFilter = (req, file, cb) => {
//     // reject a file
//     if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
//         cb(null, true);
//     } else {
//         cb(null, false);
//     }
// };
// const upload = multer({
//     storage: storage, // chỉ ra nơi lưu trữ
//     // limits: {
//     //     fileSize: 1024 * 1024 * 5
//     // },
//     fileFilter: fileFilter
// });

// const storageAudio = multer.diskStorage({
//     destination: function(req, file, cb) {
//         cb(null, './uploads/audio');
//     },
//     filenameAudio: function(req, fileAudio, cb) { //đặt tên cho file
//         cb(null, Date.now() + '-' + fileAudio.originalname);
//     }
// });
// const fileFilterAudio = (req, fileAudio, cb) => {
//     // reject a file
//     if (fileAudio.mimetype === 'audio/mpeg' || fileAudio.mimetype === 'video/mp4') {
//         cb(null, true);
//     } else {
//         cb(null, false);
//     }
// };
// const uploadAudio = multer({
//     storage: storageAudio, // chỉ ra nơi lưu trữ

//     fileFilter: fileFilterAudio
// });
module.exports = function() {
    // router.post("/music/create", function(req, res, next) {
    //     console.log(req.body);
    //     var link = 'https://nhacvavideo.s3.amazonaws.com/Video/';
    //     var imageLink = 'https://nhacvavideo.s3.amazonaws.com/';
    //     var newMusic = new music({
    //         musicName: req.body.musicName,
    //         musicType: req.body.musicType,
    //         lyrics: req.body.lyrics,
    //         isVideo: req.body.isVideo,
    //         linkMusic: link + req.body.linkMusic,
    //         image: `${imageLink}${req.body.image}`,
    //         comments: req.body.comments,
    //         singer: req.body.singer,
    //         country: req.body.country
    //     });
    //     newMusic.save().then((result) => {
    //         if (result) {

    //             musicService.updateType(req.body.musicType, { $push: { music: result._id } }).then(() => res.send('ahihi')).catch(err => res.send(err))
    //             musicService.updateSinger(req.body.singer, { $push: { music: result._id } }).then(() => res.send('ahihi')).catch(err => res.send(err))
    //         }
    //         res.status(201).json({ message: "Add the song successfully" })
    //     }).catch(err => {
    //         res.send(err);
    //     })
    // });
    router.post("/music/create", musicController.createMusic);
    router.get("/music/:id", musicController.getMusicByID);
    router.get("/music", musicController.getAllmusic);
    router.delete("/music/delete/:id", musicController.deleteMusic);
    router.put("/music/update/:id", musicController.updateMusic);
    router.get("/music/type/:id", musicController.getAllMusicByType);
    router.get("/music/singer/:id", musicController.getAllMusicBySinger);
    router.post("/music/comment", commentController.createComment);
    router.get("/music/country/:id", musicController.getAllMusicByCountry);
    router.get("/music/list/:id", musicController.getAllMusicByList);

    return router;
}