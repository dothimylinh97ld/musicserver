var typeController = require("./../Controllers/type.controller");
var router = require("express").Router();
var type = require("../Models/type.model");

module.exports = function() {

    router.post("/type/create", typeController.createType);
    router.get("/type", typeController.getAlltype);
    router.put("/type/update/:id", typeController.updateType);
    router.get("/type/:id", typeController.getTypebyID);
    router.delete("/type/delete/:id", typeController.deleteType);
    return router;
}