var userController = require("./../Controllers/user.controller");
var router = require("express").Router();
const mongoose = require("mongoose");
var user = require("./../Models/user.model");
module.exports = function() {
    router.post("/user/create", userController.register);
    router.put("/user/update/:id", userController.updateUser);
    router.get("/user/get/profile", userController.getAllUser);
    router.post("/user/sign", userController.sign_in);
    router.get("/user/get/:id", userController.getUserByID);
    router.delete("/user/delete/:id", userController.deleteUser);
    return router;
}