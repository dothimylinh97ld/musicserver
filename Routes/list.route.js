var listController = require("../Controllers/list.controller");

var router = require("express").Router();

module.exports = function() {
    router.post("/list/create", listController.createList);
    router.get("/list/all", listController.getAllList);
    router.get("/list/user/:id", listController.getListByUser);
    router.put("/list/:id", listController.updateList);
    router.get("/list/:id", listController.getListByID);
    return router;
}