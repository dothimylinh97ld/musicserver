var countryController = require("./../Controllers/country.controller");
var router = require("express").Router();
var country = require("../Models/country.model");

module.exports = function() {

    router.post("/country/create", countryController.createCountry);
    router.get("/country", countryController.getAllCountry);
    router.put("/country/update", countryController.updateCountry);
    router.get("/country/:id", countryController.getCountrybyID);
    return router;
}