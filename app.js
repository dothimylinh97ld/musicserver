var db = require("./db");
var express = require("express");

var userRoutes = require('./Routes/user.route');
var music = require("./Models/music.model");
var musicRoutes = require('./Routes/music.route');
var listRoutes = require('./Routes/list.route');
var singerRoutes = require('./Routes/singer.route');
var typeRoutes = require('./Routes/type.route');
var countryRoutes = require('./Routes/country.route');
var commentRoutes = require('./Routes/comment.route');
var musicTempRoutes = require('./Routes/musicTemp.route');
//khởi tạo server
const bodyParser = require("body-parser");
var app = express();

const PORT = process.env.PORT || 5000; // process.env là 1 object chưa tất cả các thông tin về môi trường mà nodejs đang chạy
app.use(express.static(__dirname + '/uploads'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
jsonwebtoken = require("jsonwebtoken");
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}
app.use(allowCrossDomain);

app.use('/', userRoutes());
app.use('/', musicRoutes());
app.use('/', listRoutes());
app.use('/', singerRoutes());
app.use('/', typeRoutes());
app.use('/', countryRoutes());
app.use('/', commentRoutes());
app.use('/', musicTempRoutes());
app.listen(PORT, function() {
    console.log("Listening on " + PORT);
});

app.get("/", (req, res) => {
    res.send("listening on " + PORT);
});