var singer = require('../Models/singer.model');
var user = require('../Models/user.model');
var music = require('../Models/music.model');
var type = require('../Models/type.model');
var country = require('../Models/country.model');
var listMusic = require('../Models/list.model');
var comment = require('../Models/comment.model');
var musicTemp = require('../Models/musicTemp.model');
module.exports = {
    updateSinger: updateSinger,
    updateUser: updateUser,
    getOneMusicbyID: getOneMusicbyID,
    getListByID: getListByID,
    getAllMusic: getAllMusic,
    getAllMusicTemp: getAllMusicTemp,
    deleteMusic: deleteMusic,
    updateMusic: updateMusic,
    getMusicbyType: getMusicbyType,
    getSingerbyID: getSingerbyID,
    getAllSinger: getAllSinger,
    deleteSinger: deleteSinger,
    getAllType: getAllType,
    getAllCountry: getAllCountry,
    getUserbyID: getUserbyID,
    deleteUser: deleteUser,
    getMusicbySinger: getMusicbySinger,
    updateType: updateType,
    updateCountry: updateCountry,
    updatelistMusic: updatelistMusic,
    updateComment: updateComment,
    getTypebyID: getTypebyID,
    getCountrybyID: getCountrybyID,
    getMusicbyCountry: getMusicbyCountry,
    getSingerbyMusic: getSingerbyMusic,
    getListByUser: getListByUser,
    getAllMusicByList: getAllMusicByList,
    getCommentbyMusic: getCommentbyMusic,
    getCommentbyUser: getCommentbyUser,
    deleteType: deleteType
}


function updateSinger(id, singerData) {
    return singer.findByIdAndUpdate(id, singerData);
}

function updateUser(id, userData) {
    return user.findByIdAndUpdate(id, userData);
}

function updatelistMusic(id, listData) {
    return listMusic.findByIdAndUpdate(id, listData);
}

function getAllMusic(callback) {
    music.find({}).populate('singer').exec((err, musics) => {
        if (err) {
            callback(err);
        } else {
            callback(null, musics);
        }
    });
}

function getAllMusicTemp(callback) {
    musicTemp.find({}).exec((err, musics) => {
        if (err) {
            callback(err);
        } else {
            callback(null, musics);
        }
    });
}

function getAllType(callback) {
    type.find({}).exec((err, types) => {
        if (err) {
            callback(err);
        } else {
            callback(null, types);
        }
    });
}

function getAllCountry(callback) {
    country.find({}).exec((err, types) => {
        if (err) {
            callback(err);
        } else {
            callback(null, types);
        }
    });
}

function getMusicbyCountry(id) {
    return new Promise((res, rej) => {
        country.findById(id).populate('music').exec(function(err, dataMusics) {
            if (err) {
                rej(err);
            } else {
                if (!dataMusics) {
                    rej({
                        statusCode: 400,
                        message: 'Không lấy được nhạc'
                    })
                } else {
                    res(dataMusics);
                }
            }

        })

    });
}

async function getAllSinger() {
    return await singer.find({});
}
//ahihi
function getMusicbySinger(id) {
    return new Promise((res, rej) => {
        singer.findById(id).populate('music').exec(function(err, dataMusics) {
            if (err) {
                rej(err);
            } else {
                if (!dataMusics) {
                    rej({
                        statusCode: 400,
                        message: 'Không lấy được nhạc'
                    })
                } else {
                    res(dataMusics);
                }
            }
        })
    });
}

function getCommentbyMusic(id) {
    return new Promise((res, rej) => {
        music.findById(id).populate({ path: 'comment', populate: { path: 'user' } }).populate({ path: 'singer' }).populate({ path: 'musicType' }).exec(function(err, dataComment) {
            if (err) {
                rej(err);
            } else {
                if (!dataComment) {
                    rej({
                        statusCode: 400,
                        message: 'Không lấy được nhạc'
                    })
                } else {
                    res(dataComment);
                }
            }
        })
    });
}

function getCommentbyUser(id) {
    return new Promise((res, rej) => {
        user.findById(id).populate('comment').exec(function(err, dataComment) {
            if (err) {
                rej(err);
            } else {
                if (!dataComment) {
                    rej({
                        statusCode: 400,
                        message: 'Không lấy được nhạc'
                    })
                } else {
                    res(dataComment);
                }
            }
        })
    });
}

function getMusicbyType(id) {
    return new Promise((res, rej) => {
        type.findById(id).populate('music').exec(function(err, dataMusics) {
            if (err) {
                rej(err);
            } else {
                if (!dataMusics) {
                    rej({
                        statusCode: 400,
                        message: 'Không lấy được nhạc'
                    })
                } else {
                    // dataMusics.singer = dataMusics.singer.map((data) => {
                    //     singer.findById(data).exec((err, dataSinger) => {
                    //         return dataSinger
                    //     })
                    // })
                    res(dataMusics);
                    // singer.populate(Object.keys(dataMusics)[1]).exec((err, dataSinger) => {
                    //     if (!dataSinger) {
                    //         rej({
                    //             statusCode: 400,
                    //             message: 'Không lấy được nhạc'
                    //         })
                    //     } else {
                    //         dataMusics.singer = dataSinger;
                    //         res(dataMusics);
                    //     }
                    // })

                }
            }

        })

    });
}

function getAllMusicByList(id) {
    return new Promise((res, rej) => {
        listMusic.findById(id).populate('music').exec(function(err, dataListMusic) {
            if (err) {
                rej(err);
            } else {
                if (!dataListMusic) {
                    rej({
                        statusCode: 400,
                        message: 'Không lấy được nhạc'
                    })
                } else {
                    res(dataListMusic);
                }
            }
        })
    });
}

function getListByUser(id) {
    return new Promise((res, rej) => {
        user.findById(id).populate({ path: 'listMusic' }).exec(function(err, dataListMusic) {
            if (err) {
                rej(err);
            } else {
                if (!dataListMusic) {
                    rej({
                        statusCode: 400,
                        message: 'Không lấy được list nhạc'
                    })
                } else {
                    res(dataListMusic);
                }
            }
        })
    });
}

function getSingerbyMusic(id) {
    return new Promise((res, rej) => {
        music.findById(id).populate('singer').exec(function(err, dataSingers) {
            if (err) {
                rej(err);
            } else {
                if (!dataSingers) {
                    rej({
                        statusCode: 400,
                        message: 'Không lấy được ca sĩ'
                    })
                } else {
                    res(dataSingers);
                }
            }

        })

    });
}
// populate({ path: 'comment', populate: { path: 'user' } })
function getOneMusicbyID(id) {
    return new Promise((res, rej) => {
        music.findById(id).populate({ path: 'singer', populate: { path: 'musicType' } }).exec((err, musicData) => {
            if (err) {
                rej(err);
            } else {
                if (!musicData) {
                    rej({
                        statusCode: 400,
                        message: 'Không lấy được nhạc'
                    })
                } else {
                    console.log(musicData);

                    res(musicData);
                }
            }
        });
    });
}

function getListByID(id) {
    return new Promise((res, rej) => {
        listMusic.findById(id).populate({ path: 'music' }).exec((err, musicList) => {
            if (err) {
                rej(err);
            } else {
                if (!musicList) {
                    rej({
                        statusCode: 400,
                        message: 'Không lấy được nhạc'
                    })
                } else {
                    console.log(musicList);
                    res(musicList);
                }
            }
        });
    });
}

function getTypebyID(id) {
    return new Promise((res, rej) => {
        type.findById(id).populate('music').exec((err, typeData) => {
            if (err) {
                rej(err);
            } else {
                if (!typeData) {
                    rej({
                        statusCode: 400,
                        message: 'Không lấy được thể loại'
                    })
                } else {
                    res(typeData);
                }
            }
        });
    });
}

function getCountrybyID(id) {
    return new Promise((res, rej) => {
        country.findById(id).exec((err, countryData) => {
            if (err) {
                rej(err);
            } else {
                if (!countryData) {
                    rej({
                        statusCode: 400,
                        message: 'Không lấy được Quốc gia'
                    })
                } else {
                    res(countryData);
                }
            }
        });
    });
}

function getUserbyID(id) {
    return new Promise((res, rej) => {
        user.findOne({ _id: id }).exec((err, userData) => {
            if (err) {
                rej(err);
            } else {
                if (!userData) {
                    rej({
                        statusCode: 400,
                        message: 'Không lấy được user'
                    })
                } else {
                    res(userData);
                }
            }
        });
    });
}

function getSingerbyID(id) {
    return new Promise((res, rej) => {
        singer.findOne({ _id: id }).exec((err, singerData) => {
            if (err) {
                rej(err);
            } else {
                if (!singerData) {
                    rej({
                        statusCode: 400,
                        message: 'Không có nghệ sĩ này'
                    })
                } else {
                    res(singerData);
                }
            }
        });
    });
}



function deleteMusic(id) {
    return new Promise((res, rej) => {
        music.find({ _id: id }).exec((err, musicData) => {
            if (err) {
                rej(err);
            } else {
                if (!musicData) {
                    rej({
                        statusCode: 400,
                        message: "Lỗi khi xóa bai, không có bài hát này"
                    });
                } else {
                    music.remove({
                        _id: id
                    }).exec((err, response) => {
                        if (err) {
                            rej(err);
                        } else {
                            res({
                                message: "Xóa bài hát thành công"
                            });
                        }
                    });
                }
            }
        });
    });
}

function deleteType(id) {
    return new Promise((res, rej) => {
        type.find({ _id: id }).exec((err, typeData) => {
            if (err) {
                rej(err);
            } else {
                if (!typeData) {
                    rej({
                        statusCode: 400,
                        message: "Lỗi khi xóa thể loại, không có thể loại này"
                    });
                } else {
                    type.remove({
                        _id: id
                    }).exec((err, response) => {
                        if (err) {
                            rej(err);
                        } else {
                            res({
                                message: "Xóa thể loại thành công"
                            });
                        }
                    });
                }
            }
        });
    });
}

function deleteUser(id) {
    return new Promise((res, rej) => {
        user.find({ _id: id }).exec((err, userData) => {
            if (err) {
                rej(err);
            } else {
                if (!userData) {
                    rej({
                        statusCode: 400,
                        message: "Lỗi khi xóa người dùng, không có người này"
                    });
                } else {
                    user.remove({
                        _id: id
                    }).exec((err, response) => {
                        if (err) {
                            rej(err);
                        } else {
                            res({
                                message: "Xóa người dùng thành công"
                            });
                        }
                    });
                }
            }
        });
    });
}

function updateMusic(id, musicData) {
    return music.findByIdAndUpdate(id, musicData);
}

function updateComment(id, commentData) {
    return comment.findByIdAndUpdate(id, commentData);
}

function updateType(id, typeData) {
    return type.findByIdAndUpdate(id, typeData);
}


function updateCountry(id, CountryData) {
    return type.findByIdAndUpdate(id, CountryData);
}

function deleteSinger(id) {
    return new Promise((res, rej) => {
        singer.find({ _id: id }).exec((err, singerData) => {
            if (err) {
                rej(err);
            } else {
                if (!singerData) {
                    rej({
                        statusCode: 400,
                        message: "Lỗi khi xóa nghệ sĩ, không có nghệ sĩ này"
                    });
                } else {
                    singer.remove({
                        _id: id
                    }).exec((err, response) => {
                        if (err) {
                            rej(err);
                        } else {
                            res({
                                message: "Xóa nghệ sĩ thành công"
                            });
                        }
                    });
                }
            }
        });
    });
}