var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var commentSchema = new Schema({
    comment: {
        type: String,
        trim: true
    },
    emotion: {
        type: Boolean,
        default: false
    },
    music: {
        type: Schema.Types.ObjectId,
        ref: 'music'
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
});

var comment = mongoose.model('comment', commentSchema);

module.exports = comment;