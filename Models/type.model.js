var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var type = new Schema({
    musicType: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },
    nameType: {
        type: String,
        trim: true,
        required: true
    },
    image: {
        type: String,
        trim: true,
        required: true
    },
    music: [{
        type: Schema.Types.ObjectId,
        ref: 'music'
    }]
});

var type = mongoose.model('type', type);
module.exports = type;