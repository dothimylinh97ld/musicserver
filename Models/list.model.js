var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var listSchema = new Schema({
    listName: {
        type: String,
        trim: true,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    music: [{
        type: Schema.Types.ObjectId,
        ref: 'music'
    }]
});

var listMusic = mongoose.model('listMusic', listSchema);

module.exports = listMusic;