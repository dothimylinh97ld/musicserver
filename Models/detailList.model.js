var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var detailListSchema = new Schema({
    nameDetail: {
        type: String,
        trim: true
    },
    music: [{
        type: Schema.Types.ObjectId,
        ref: 'music'
    }],
    listMusic: {
        type: Schema.Types.ObjectId,
        ref: 'listMusic'
    }
});
var detailList = mongoose.model('detailList', detailListSchema);
module.exports = detailList;