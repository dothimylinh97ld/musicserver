var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var singerSchema = new Schema({
    singerName: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },
    nation: {
        type: String,
        trim: true,
        required: true
    },
    avatar: {
        type: String,
        trim: true,
        required: true
    },
    story: {
        type: String,
        trim: true
    },
    music: [{
        type: Schema.Types.ObjectId,
        ref: 'music'
    }]
});


var singer = mongoose.model('singer', singerSchema);
module.exports = singer;