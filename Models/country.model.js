var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var country = new Schema({
    countryName: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },
    music: [{
        type: Schema.Types.ObjectId,
        ref: 'music'
    }]
});

var country = mongoose.model('country', country);
module.exports = country;