var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    fullName: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },
    email: {
        type: String,
        unique: true,
        lowercase: true,
        trim: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    role_: {
        type: String,
        trim: true,
        default: '0'
    },
    avata: {
        type: String,
        trim: true,
        required: true
    },
    listMusic: [{
        type: Schema.Types.ObjectId,
        ref: 'listMusic'
    }],
    comment: [{
        type: Schema.Types.ObjectId,
        ref: 'comment'
    }],

});


var user = mongoose.model('user', UserSchema);
module.exports = user;