var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var user = require("./user.model");

var musicSchema = new Schema({
    musicName: {
        type: String,
        trim: true,
        required: true,
    },
    musicType: {
        type: Schema.Types.ObjectId,
        ref: 'type'
    },

    lyrics: {
        type: String
    },
    isVideo: {
        type: String,
        trim: true,
        required: true
    },
    linkMusic: {
        type: String,
        trim: true,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    country: {
        type: String,
        required: true
    },
    comment: [{
        type: Schema.Types.ObjectId,
        ref: 'comment'
    }],
    singer: [{
        type: Schema.Types.ObjectId,
        ref: 'singer'
    }],

    listMusic: [{
        type: Schema.Types.ObjectId,
        ref: 'listMusic'
    }]

});
var music = mongoose.model('music', musicSchema);

module.exports = music;