var mongoose = require("mongoose");
var comment = require("./../Models/comment.model");
var musciService = require("./../Services/music.service")
exports.createComment = async(req, res, next) => {
    // var newMusic = new music(req.body);
    try {
        const newCmt = new comment({
            comment: req.body.comment,
            emotion: req.body.emotion,
            music: req.body.music,
            user: req.body.user,
        });
        console.log(newCmt);

        const created = await comment.create(newCmt);
        console.log(created);

        if (created) {
            musciService.updateMusic(req.body.music, { $push: { comment: created._id } }).then(data => res.send(data)).catch(err => res.send(err));
            musciService.updateUser(req.body.user, { $push: { comment: created._id } }).then(data => res.send(data)).catch(err => res.send(err));
            return res.status(200).send(created);
            console.log("Thành công");

        }
        return res.status(400).json({
            statusCode: 400,
            message: 'Du lieu gui len thieu cai truong gi do '
        });
    } catch (error) {
        // cai cho nay neu co loi thi o duoi angular co the log ra, va hien thong bao loi thong qua cai message kia
        return res.status(500).json({ statusCode: 500, message: error.message });
    }
};
// exports.createComment = function(req, res, next) {
//     console.log(req.body);
//     var newComment = new comment({
//         comment: req.body.comment,
//         emotion: req.body.emotion,
//         music: req.body.music,
//         user: req.body.user,
//     });
//     newComment.save().then((result) => {
//         if (result) {
//             musciService.updateMusic(req.body.music, { $push: { comment: created._id } }).then(data => res.send(data)).catch(err => res.send(err));
//             musciService.updateUser(req.body.user, { $push: { comment: created._id } }).then(data => res.send(data)).catch(err => res.send(err));
//         }
//         res.status(200).json({ message: "Add the comment successfully" })
//     }).catch(err => {
//         res.send(err);
//     })
// };
exports.getCommentbyMusic = function(req, res, next) {
    let idMusic = req.params.id;
    if (!idMusic) {
        res.status.send({
            message: "Không có bài hát này"
        })
    }
    musciService.getCommentbyMusic(idMusic).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(400).send(err);
    })
}

exports.getCommentByUser = function(req, res, next) {
    let idUser = req.params.id;
    if (!idUser) {
        res.status.send({
            message: "Không có người dùng này"
        })
    }
    musciService.getCommentbyUser(idUser).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(400).send(err);
    })
}

exports.updateComment = function(req, res, next) {
    var _id = req.params.id;
    var commentData = req.body;

    musciService.updateComment(_id, commentData).then(() => {
        res.send(commentData);
    }).catch((err) => {
        res.status(400).send(err);
    });
}