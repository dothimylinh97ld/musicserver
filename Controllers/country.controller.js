var country = require("../Models/country.model");
var musicService = require("./../Services/music.service");


exports.getAllCountry = function(req, res, next) {
    musicService.getAllCountry((err, response) => {
        if (err) {
            res.status(400).send(err);
        } else {
            res.send(response);
        }
    });
};

exports.updateCountry = function(req, res, next) {
    var _id = req.params.id;
    var countryData = req.body;

    musicService.updateCountry(_id, countryData).then(() => {
        res.send(countryData);
    }).catch((err) => {
        res.status(400).send(err);
    });
}

exports.getCountrybyID = function(req, res, next) {
    let id = req.params.id;
    if (!id) {
        res.status(400).send({
            message: "Không có Quốc gia này"
        });
    }
    musicService.getCountrybyID(id).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(400).send(err);
    })
}

exports.createCountry = function(req, res, next) {
    console.log(req.body);

    var newCountry = new country({
        countryName: req.body.countryName,
        type: req.body.type,
        singer: req.body.singer
    });
    newCountry.save().then((result) => {
        res.status(200).json({
            message: "Add the newCountry successfully"
        })
    }).catch(err => {
        res.send(err);
    });
};