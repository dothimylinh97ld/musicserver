var singer = require("../Models/singer.model");
var musicService = require("./../Services/music.service");

exports.createSinger = function(req, res, next) {
    console.log(req.body);
    var link = 'https://nhacvavideo.s3.amazonaws.com/Images/';

    var newSinger = new singer({
        singerName: req.body.singerName,
        nation: req.body.nation,
        avatar: `${link}${req.body.avatar}`,
        story: req.body.story,
        music: req.body.music
    });
    newSinger.save().then((result) => {
        if (result) {
            musicService.updateMusic(req.body.music, { $push: { singer: result._id } }).then(() => res.send('Thành công')).catch(err => res.send(err))
        }
        res.status(200).json({
            message: "Add singer successfully"
        })
    }).catch(err => {
        console.log(err);
        res.send(err);
    })
};

exports.updateSinger = function(req, res, next) {
    var _id = req.params.id;
    var singer_data = req.body;

    musicService.updateSinger(_id, singer_data).then(() => {
        res.send(singer_data)
    }).catch((err) => {
        res.status(400).send(err);
    });
}

exports.getOneSinger = function(req, res, next) {
    let id = req.params.id;
    if (!id) {
        res.status(400).send({
            message: "Không có nghệ sĩ, ca sĩ này"
        });
    }
    musicService.getSingerbyID(id).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(400).send(err);
    })
}

exports.getAllSinger = async function(req, res, next) {
    const result = await musicService.getAllSinger();
    if (result) return res.status(200).send(result);
    return res.status(400).json({ message: 'Errors' });
};

exports.deleteSinger = function(req, res, next) {
    let id = req.params.id;

    musicService.deleteSinger(id).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(err.statusCode).send(err);
    });
}

exports.getSingerByMusic = function(req, res, next) {
    let idMusic = req.params.id;
    if (!idMusic) {
        res.status.send({
            message: err.message
        })
    }
    musicService.getSingerbyMusic(idMusic).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(400).send(err);
    });
}