var mongoose = require("mongoose");
var jwt = require("jsonwebtoken");
var User = require("../Models/user.model");
var musicService = require("./../Services/music.service");
exports.register = function(req, res) {
    console.log(req.body);

    User.findOne({ email: req.body.email }).then(result => {
        if (!result) {
            // var link = 'https://nhacvavideo.s3.amazonaws.com/Images/';
            var link = 'https://nhacvavideo.s3.amazonaws.com/Images/user.png';

            var newUser = new User({
                fullName: req.body.fullName,
                email: req.body.email,
                password: req.body.password,
                role_: req.body.role_,
                // avata: `${link}${req.body.avata}`,
                avata: link,
                listMusic: req.body.listMusic
            });
            newUser.save().then((result) => {
                if (result) {
                    musicService.updatelistMusic(req.body.listMusic, { $push: { user: result._id } }).then(() => res.send('ahihi')).catch(err => res.send(err))
                }
                res.status(200).json({
                    message: "Add the list successfully"
                })
            }).catch(err => {
                return res.send(err);
            });
        } else {
            return res.status(404).send({ message: "Email da ton tai. Kiem tra lai" });
        }
    });
};

exports.getAllUser = async function(req, res) {
    try {
        const users = await User.find().select('-password');
        if (users) {
            return res.status(200).send(users);
        }
        return res.status(404).json({ statusCode: 404, message: 'Not found' });
    } catch (error) {
        return res.status(500).json({ statusCode: 500, message: error.message });
    }
};
exports.sign_in = function(req, res) {
    console.log(req.body);

    User.findOne({ email: req.body.email }).then(user => {
        if (!user) {
            return res.status(404).send("Not found");
        } else {
            if (user.password === req.body.password) {
                return res.status(200).send(user);
            } else {
                return res
                    .status(404)
                    .send({ message: "Thong tin khong chinh xac. Kiem tra lai !!!" });
            }
        }
    });
};

exports.loginRequired = function(req, res, next) {
    if (req.user) {
        next();
    } else {
        return res.status(401).json({ message: "Unauthorized user!" });
    }
};

exports.updateUser = function(req, res, next) {
    var _id = req.params.id;
    var user_data = req.body;

    musicService.updateUser(_id, user_data).then(() => {
        res.send(user_data)
    }).catch((err) => {
        res.status(400).send(err);
    });
}

exports.getUserByID = function(req, res, next) {
    let id = req.params.id;
    if (!id) {
        res.status(400).send({
            message: err.message
        });
    }
    musicService.getUserbyID(id).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(400).send(err);
    });
}
exports.deleteUser = function(req, res, next) {
    let id = req.params.id;

    musicService.deleteUser(id).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(err.statusCode).send(err);
    });
}