var music = require("../Models/music.model");
var message = require("./../util/message");
var musicService = require("./../Services/music.service");

exports.createMusic = function(req, res, next) {
    console.log(req.body);
    var link = 'https://nhacvavideo.s3.amazonaws.com/Video/';
    var imageLink = 'https://nhacvavideo.s3.amazonaws.com/';
    var newMusic = new music({
        musicName: req.body.musicName,
        musicType: req.body.musicType,
        lyrics: req.body.lyrics,
        isVideo: req.body.isVideo,
        linkMusic: link + req.body.linkMusic,
        image: `${imageLink}${req.body.image}`,
        comments: req.body.comments,
        singer: req.body.singer,
        country: req.body.country,
        listMusic: req.body.listMusic
    });
    newMusic.save().then((result) => {
        if (result) {
            musicService.updateType(req.body.musicType, { $push: { music: result._id } }).then(() => res.send('Thành công')).catch(err => res.send(err))
            musicService.updateSinger(req.body.singer, { $push: { music: result._id } }).then(() => res.send('Thành công')).catch(err => res.send(err))
            musicService.updatelistMusic(req.body.listMusic, { $push: { music: result._id } }).then(() => res.send('Thành công')).catch(err => res.send(err))
        }
        res.status(200).json({ message: "Add the song successfully" })
    }).catch(err => {
        res.send(err);
    })
};
exports.getAllMusicByCountry = function(req, res, next) {
    musicService.getMusicbyCountry(req.params.id).then((data) => {

        res.send(data)
    }).catch((err) => res.send(err));
}


exports.getAllmusic = function(req, res, next) {
    musicService.getAllMusic((err, response) => {
        if (err) {
            res.status(400).send(err);
        } else {
            res.send(response);
        }
    });
};
// exports.getAllmusicBySinger = function(req, res, next) {
//     musicService.getAllMusicBySinger((err, response) => {
//         if (err) {
//             res.status(400).send(err);
//         } else {
//             res.send(response);
//         }
//     });
// };
exports.getMusicByID = function(req, res, next) {
    let id = req.params.id;
    if (!id) {
        res.status(400).send({
            message: err.message
        });
    }
    musicService.getOneMusicbyID(id).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(400).send(err);
    });
}
exports.getAllMusicBySinger = function(req, res, next) {
    let idSinger = req.params.id;
    if (!idSinger) {
        res.status.send({
            message: "Không có ca sĩ này"
        })
    }
    musicService.getMusicbySinger(idSinger).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(400).send(err);
    })
}

exports.getAllMusicByType = function(req, res, next) {
    let idType = req.params.id;
    if (!idType) {
        res.status.send({
            message: err.message
        })
    }
    musicService.getMusicbyType(idType).then((response) => {

        res.send(response);
    }).catch((err) => {
        res.status(400).send(err);
    })
}

exports.getAllMusicByList = function(req, res, next) {
    let idList = req.params.id;
    if (!idList) {
        res.status.send({
            message: err.message
        })
    }
    musicService.getAllMusicByList(idList).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(400).send(err);
    })
}

exports.deleteMusic = function(req, res, next) {
    let id = req.params.id;

    musicService.deleteMusic(id).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(err.statusCode).send(err);
    });
}

exports.updateMusic = function(req, res, next) {
    var _id = req.params.id;
    var musicData = req.body;
    // console.log(_id + "\t" + musicData);

    musicService.updateMusic(_id, musicData).then(() => {
        res.send(musicData);
    }).catch((err) => {
        res.status(400).send(err);
    });
}