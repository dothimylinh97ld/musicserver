var list = require("../Models/list.model");
var musicService = require('../Services/music.service')
exports.createList = function(req, res) {
    var newList = new list({
        listName: req.body.listName,
        user: req.body.user,
        music: req.body.music
    });
    newList.save().then((result) => {
        if (result) {
            musicService.updateMusic(req.body.music, { $push: { listMusic: result._id } }).then(() => res.send('ahihi')).catch(err => res.send(err))
            musicService.updateUser(req.body.user, { $push: { listMusic: result._id } }).then(() => res.send('ahihi')).catch(err => res.send(err))
        }
        res.status(200).json({
            message: "Add the list successfully"
        })
    }).catch(err => {
        return res.send(err);
    });
};

exports.getAllList = function(req, res) {
    list.find().then((list) => {
        if (!list) {
            return res.status(400).send(err);
        } else {
            res.send(list);
        }
    }).catch((err) => {
        res.send(err)
    });
};

exports.getListByUser = function(req, res, next) {
    let idUser = req.params.id;
    if (!idUser) {
        res.status.send({
            message: err.message
        })
    }
    musicService.getListByUser(idUser).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(400).send(err);
    })
}

exports.updateList = function(req, res, next) {
    let idList = req.params.id;
    let listData = req.body;

    musicService.updatelistMusic(idList, listData).then(() => {
        res.send(listData);
    }).catch((err) => {
        res.status(400).send(err);
    });
}

exports.getListByID = function(req, res, next) {
    let id = req.params.id;
    if (!id) {
        res.status(400).send({
            message: err.message
        });
    }
    musicService.getListByID(id).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(400).send(err);
    });
}