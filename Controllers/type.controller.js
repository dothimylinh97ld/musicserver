var type = require("../Models/type.model");
var musicService = require("./../Services/music.service");


exports.getAlltype = function(req, res, next) {
    musicService.getAllType((err, response) => {
        if (err) {
            res.status(400).send(err);
        } else {
            res.send(response);
        }
    });
};

exports.updateType = function(req, res, next) {
    var _id = req.params.id;
    var typeDate = req.body;

    musicService.updateType(_id, typeDate).then(() => {
        res.send(typeDate);
    }).catch((err) => {
        res.status(400).send(err);
    });
}

exports.getTypebyID = function(req, res, next) {
    let id = req.params.id;
    if (!id) {
        res.status(400).send({
            message: "Không có thể loại này"
        });
    }
    musicService.getTypebyID(id).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(400).send(err);
    })
}

exports.createType = function(req, res, next) {
    console.log(req.body);
    var imageLink = 'https://nhacvavideo.s3.amazonaws.com/';
    var newType = new type({
        musicType: req.body.musicType,
        nameType: req.body.nameType,
        image: `${imageLink}${req.body.image}`,
        music: req.body.music
    });
    newType.save().then((result) => {
        if (result) {
            musicService.updateMusic(req.body.music, { $push: { type: result._id } }).then(() => res.send('ahihi')).catch(err => res.send(err))
        }
        res.status(200).json({
            message: "Add the type successfully"
        })
    }).catch(err => {
        res.send(err);
    });
};

exports.deleteType = function(req, res, next) {
    let id = req.params.id;

    musicService.deleteType(id).then((response) => {
        res.send(response);
    }).catch((err) => {
        res.status(err.statusCode).send(err);
    });
}