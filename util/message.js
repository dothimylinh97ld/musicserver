module.exports = {
    ERROR_MESSAGE: {
        MUSIC: {
            EXIST: "MUSIC_EXIST",
            NOT_FOUND: "MUSIC_NOT_FOUND",
            INVALD: "MUSIC_INVALID"
        },
        USER: {
            EXIST: 'USER_EXIST',
            NOT_FOUND: 'USER_NOT_FOUND',
            INVALD: 'USER_INVALID'
        }
    },
    SUCCESS_MESSAGE: {
        MUSIC: {
            CREATED: "MUSIC_CREATED",
            DELETED: "MUSIC_DELETED"
        },
        USER: {
            CREATED: 'USER_CREATED',
            DELETED: 'USER_DELETED'
        }
    }
};